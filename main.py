# from typing import Any, Dict
#
# from appium import webdriver
# from selenium.webdriver.support.ui import WebDriverWait
# from appium.options import AppiumOptions
# from tests.InvalidPhoneNo import test_invalid_phone
# from tests.buyerRegistration import test_valid_registration
# from tests.existingEmail import test_already_registered_email
# from tests.invalidEmailReg import test_invalid_email
# from tests.existingPhoneNo import test_already_registered_phone
#
#
# def setup_driver():
#     cap: Dict[str, Any] = dict(platformName="Android", platformVersion="11", deviceName="Pixel 8 Pro",
#                                automationName="UIAutomator2", noReset=True, fullReset=False,
#                                app="C://jiji-ng-4-9-0-2.apk", androidInstallTimeout=120000)
#
#     url = "http://localhost:4723"
#     options = AppiumOptions()
#     options.add_experimental_option('w3c', False)
#     options.update(capabilities=cap)
#
#     driver = webdriver.Remote(url, options=options)
#     return driver
#
#
# def main():
#     driver = setup_driver()
#     wait = WebDriverWait(driver, 30)
#
#     # Run test cases
#     try:
#         test_valid_registration(driver, wait)
#         test_invalid_email(driver, wait)
#         test_invalid_phone(driver, wait)
#         test_already_registered_email(driver, wait)
#         test_already_registered_phone(driver, wait)
#     except Exception as e:
#         print(f"Error occurred during test execution: {str(e)}")
#     finally:
#         driver.quit()
#
#
# if __name__ == "__main__":
#     main()
