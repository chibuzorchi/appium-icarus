# Use the official Python base image
FROM python:3.9

# Install Appium-Python-Client and other dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

# Set up the working directory inside the container
WORKDIR /app

# Copy your test scripts and other necessary files into the container
COPY . .

# Command to run your tests (adjust as per your project structure)
CMD ["python", "tests/test_valid_registration.py", "tests/buyerRegistration.py", "tests/existingEmail.py", "tests/existingPhoneNo.py", "tests/invalidEmailReg.py", "tests/InvalidPhoneNo.py"]
